**Henderson free obgyn clinic**

The Henderson Free Obgyn Clinic is the country's first integrated free female health clinic coordinated by students.
Please Visit Our Website [Henderson free obgyn clinic](https://obgynhenderson.com/free-obgyn-clinic.php) for more information.

---

## Our free obgyn clinic inHenderson services

The Henderson Free OBGYN Clinic is a partnership between New York University's School of Medicine, the Reproductive 
Health Access Program, and the Family Health Center. The Women's Health Free Clinic is open two Saturdays a month 
please call for a timetable. 

The Free Clinic for Women's Wellness offers: 

Routine GYN/well-woman exam routine 
Pap smears, checking for HPV 
Anticonception
Testing for STD and therapy 
Testing for conception and therapy 
For eligible patients, free IUDs and contraceptive implants 
And a lot more!

